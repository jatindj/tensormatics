import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  active: any;
  constructor() { }
  ngOnInit(): void {
    this.active = 1;
  }
  onNotify(message: string) {
    if (message === 'success') {
      if (this.active >= 6) {
        this.active = 1;
      } else {
        this.active++;
      }
    }
  }

}

