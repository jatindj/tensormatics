import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-imagelink',
  templateUrl: './imagelink.component.html',
  styleUrls: ['./imagelink.component.css']
})
export class ImagelinkComponent implements OnInit {

  cactive: any;
  @Input() accessPID: number;
  @Output() notifyparent: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.cactive = 1;
    setInterval(() => this.changechildID(), 1000);
  }

  changechildID() {
    if (this.accessPID === 1) {
      if (this.cactive === 6) {
        this.cactive = 1;
        return this.notifyparent.emit('success');
      } else {
        this.cactive++;
      }
    }
  }
}
