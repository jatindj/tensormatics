import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentlinkComponent } from './documentlink.component';

describe('DocumentlinkComponent', () => {
  let component: DocumentlinkComponent;
  let fixture: ComponentFixture<DocumentlinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentlinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentlinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
