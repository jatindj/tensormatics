import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-documentlink',
  templateUrl: './documentlink.component.html',
  styleUrls: ['./documentlink.component.css']
})
export class DocumentlinkComponent implements OnInit {

  dactive: any;
  @Input() accessPID: number;
  @Output() notifyparent: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.dactive = 1;
    setInterval(() => this.changechildID(), 1000);
  }

  changechildID() {
    if (this.accessPID === 3) {
      if (this.dactive === 2) {
        this.dactive = 1;
        return this.notifyparent.emit('success');
      } else {
        this.dactive++;
      }
    } else {
      alert('error');
    }
  }
}
