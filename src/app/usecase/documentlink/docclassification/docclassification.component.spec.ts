import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocclassificationComponent } from './docclassification.component';

describe('DocclassificationComponent', () => {
  let component: DocclassificationComponent;
  let fixture: ComponentFixture<DocclassificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocclassificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocclassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
