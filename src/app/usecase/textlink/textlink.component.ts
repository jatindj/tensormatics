import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-textlink',
  templateUrl: './textlink.component.html',
  styleUrls: ['./textlink.component.css']
})
export class TextlinkComponent implements OnInit {

  tactive: any;
  @Input() accessPID: number;
  @Output() notifyparent: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.tactive = 1;
    setInterval(() => this.changechildID(), 1000);
  }
  changechildID() {
    if (this.accessPID === 4) {
      if (this.tactive === 3) {
        this.tactive = 1;
        return this.notifyparent.emit('success');
      } else {
        this.tactive++;
      }
    }
  }
}
