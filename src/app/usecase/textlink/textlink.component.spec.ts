import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextlinkComponent } from './textlink.component';

describe('TextlinkComponent', () => {
  let component: TextlinkComponent;
  let fixture: ComponentFixture<TextlinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextlinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextlinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
