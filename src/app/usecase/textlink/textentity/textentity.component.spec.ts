import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextentityComponent } from './textentity.component';

describe('TextentityComponent', () => {
  let component: TextentityComponent;
  let fixture: ComponentFixture<TextentityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextentityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextentityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
