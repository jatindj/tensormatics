import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextsentimentalComponent } from './textsentimental.component';

describe('TextsentimentalComponent', () => {
  let component: TextsentimentalComponent;
  let fixture: ComponentFixture<TextsentimentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextsentimentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextsentimentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
