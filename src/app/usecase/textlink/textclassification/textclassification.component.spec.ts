import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextclassificationComponent } from './textclassification.component';

describe('TextclassificationComponent', () => {
  let component: TextclassificationComponent;
  let fixture: ComponentFixture<TextclassificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextclassificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextclassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
