import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-geolink',
  templateUrl: './geolink.component.html',
  styleUrls: ['./geolink.component.css']
})
export class GeolinkComponent implements OnInit {

  gactive: any;
  @Input() accessPID: number;
  @Output() notifyparent: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.gactive = 1;
    setInterval(() => this.changechildID(), 3000);
  }

  changechildID() {
    if (this.accessPID === 6) {
      if (this.gactive === 1) {
        this.gactive = 1;
        return this.notifyparent.emit('success');
      } else {
        this.gactive++;
      }
    }
  }
}
