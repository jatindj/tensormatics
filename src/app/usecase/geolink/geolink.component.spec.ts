import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeolinkComponent } from './geolink.component';

describe('GeolinkComponent', () => {
  let component: GeolinkComponent;
  let fixture: ComponentFixture<GeolinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeolinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeolinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
