import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectdetectComponent } from './objectdetect.component';

describe('ObjectdetectComponent', () => {
  let component: ObjectdetectComponent;
  let fixture: ComponentFixture<ObjectdetectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectdetectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectdetectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
