import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudiolinkComponent } from './audiolink.component';

describe('AudiolinkComponent', () => {
  let component: AudiolinkComponent;
  let fixture: ComponentFixture<AudiolinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudiolinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudiolinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
