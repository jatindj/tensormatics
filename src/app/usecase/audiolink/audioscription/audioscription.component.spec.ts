import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioscriptionComponent } from './audioscription.component';

describe('AudioscriptionComponent', () => {
  let component: AudioscriptionComponent;
  let fixture: ComponentFixture<AudioscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
