import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SegmentlabelComponent } from './segmentlabel.component';

describe('SegmentlabelComponent', () => {
  let component: SegmentlabelComponent;
  let fixture: ComponentFixture<SegmentlabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SegmentlabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentlabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
