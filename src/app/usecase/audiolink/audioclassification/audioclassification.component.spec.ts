import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioclassificationComponent } from './audioclassification.component';

describe('AudioclassificationComponent', () => {
  let component: AudioclassificationComponent;
  let fixture: ComponentFixture<AudioclassificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioclassificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioclassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
