import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-audiolink',
  templateUrl: './audiolink.component.html',
  styleUrls: ['./audiolink.component.css']
})
export class AudiolinkComponent implements OnInit {
  Audioactive: any;
  @Input() accessPID: number;
  @Output() notifyparent: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.Audioactive = 1;
    setInterval(() => this.changechildID(), 1000);
  }
  changechildID() {
    if (this.accessPID === 5) {
      if (this.Audioactive === 4) {
        this.Audioactive = 1;
        return this.notifyparent.emit('success');
      } else {
        this.Audioactive++;
      }
    }
  }
}
