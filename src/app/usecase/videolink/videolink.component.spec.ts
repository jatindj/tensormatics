import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideolinkComponent } from './videolink.component';

describe('VideolinkComponent', () => {
  let component: VideolinkComponent;
  let fixture: ComponentFixture<VideolinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideolinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideolinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
