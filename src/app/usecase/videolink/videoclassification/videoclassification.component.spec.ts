import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoclassificationComponent } from './videoclassification.component';

describe('VideoclassificationComponent', () => {
  let component: VideoclassificationComponent;
  let fixture: ComponentFixture<VideoclassificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoclassificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoclassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
