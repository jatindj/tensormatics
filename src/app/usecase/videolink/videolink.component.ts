import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-videolink',
  templateUrl: './videolink.component.html',
  styleUrls: ['./videolink.component.css']
})
export class VideolinkComponent implements OnInit {
  vactive;
  @Input() accessPID: number;
  @Output() notifyparent: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.vactive = 1;
    setInterval(() => this.changechildID(), 3000);
  }
  changechildID() {
    if (this.accessPID === 2) {
      if (this.vactive === 1) {
        return this.notifyparent.emit('success');
      }
    }
  }
}
