import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatasetComponent } from './dataset/dataset.component';
import { FolderdisplayComponent } from './dataset/folderdisplay/folderdisplay.component';


const routes: Routes = [
  { path: '' , redirectTo: '/dataset', pathMatch: 'full'},
  { path: 'dataset' , component: DatasetComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
