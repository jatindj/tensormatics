import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-dataset',
  templateUrl: './dataset.component.html',
  styleUrls: ['./dataset.component.css']
})
export class DatasetComponent implements OnInit {
  public selectdataset;
  showoption = false;
  constructor(private sd: FormBuilder) { }

  ngOnInit(): void {
     this.selectdataset = this.sd.group({
      S_dataset: ['dropbox']
     });
  }
  optiontoggle() {
    this.showoption = !this.showoption;
  }
}
