import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderdisplayComponent } from './folderdisplay.component';

describe('FolderdisplayComponent', () => {
  let component: FolderdisplayComponent;
  let fixture: ComponentFixture<FolderdisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderdisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderdisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
