import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { ImagelinkComponent } from './usecase/imagelink/imagelink.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BoundingboxComponent } from './usecase/imagelink/boundingbox/boundingbox.component';
import { PointComponent } from './usecase/imagelink/point/point.component';
import { SegmentationComponent } from './usecase/imagelink/segmentation/segmentation.component';
import { DocumentlinkComponent } from './usecase/documentlink/documentlink.component';
import { TextlinkComponent } from './usecase/textlink/textlink.component';
import { VideolinkComponent } from './usecase/videolink/videolink.component';
import { ClassificationComponent } from './usecase/imagelink/classification/classification.component';
import { VideoclassificationComponent } from './usecase/videolink/videoclassification/videoclassification.component';
import { DocclassificationComponent } from './usecase/documentlink/docclassification/docclassification.component';
import { OcrComponent } from './usecase/documentlink/ocr/ocr.component';
import { AudiolinkComponent } from './usecase/audiolink/audiolink.component';
import { GeolinkComponent } from './usecase/geolink/geolink.component';
import { ObjectdetectComponent } from './usecase/geolink/objectdetect/objectdetect.component';
import { AudioclassificationComponent } from './usecase/audiolink/audioclassification/audioclassification.component';
import { AudioscriptionComponent } from './usecase/audiolink/audioscription/audioscription.component';
import { SegmentlabelComponent } from './usecase/audiolink/segmentlabel/segmentlabel.component';
import { TextclassificationComponent } from './usecase/textlink/textclassification/textclassification.component';
import { TextentityComponent } from './usecase/textlink/textentity/textentity.component';
import { TextsentimentalComponent } from './usecase/textlink/textsentimental/textsentimental.component';
import { PolygonComponent } from './usecase/imagelink/polygon/polygon.component';
import { PolylineComponent } from './usecase/imagelink/polyline/polyline.component';
import { SentimentanalysisComponent } from './usecase/audiolink/sentimentanalysis/sentimentanalysis.component';
import { DatasetComponent } from './dataset/dataset.component';
import { FolderdisplayComponent } from './dataset/folderdisplay/folderdisplay.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ImagelinkComponent,
    ClassificationComponent,
    BoundingboxComponent,
    PointComponent,
    SegmentationComponent,
    DocumentlinkComponent,
    TextlinkComponent,
    VideolinkComponent,
    VideoclassificationComponent,
    DocclassificationComponent,
    OcrComponent,
    AudiolinkComponent,
    GeolinkComponent,
    ObjectdetectComponent,
    AudioclassificationComponent,
    AudioscriptionComponent,
    SegmentlabelComponent,
    TextclassificationComponent,
    TextentityComponent,
    TextsentimentalComponent,
    PolygonComponent,
    PolylineComponent,
    SentimentanalysisComponent,
    DatasetComponent,
    FolderdisplayComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
